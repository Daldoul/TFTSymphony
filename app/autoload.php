<?php

use Doctrine\Common\Annotations\AnnotationRegistry;
use Composer\Autoload\ClassLoader;

/**
 * @var ClassLoader $loader
 */
$loader = require __DIR__.'/../vendor/autoload.php';
$loader->add('FPDF', __DIR__.'/../vendor/fpdf'); //ligne à ajouter
AnnotationRegistry::registerLoader(array($loader, 'loadClass'));

return $loader;
